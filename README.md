# Symfony application with vue frontend

## Projekt Setup

**Building assets**
We need to build the assets.

`$ npm run dev`
The assets are built with npm run dev command for the development environment.

**Running the application**
We start the development server and locate to the application page.

`$ symfony serve`
We start the development server. Then we locate to the [localhost:8000/home page](http://localhost:8000/home page).

